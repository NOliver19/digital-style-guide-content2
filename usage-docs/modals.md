<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">
	
### What are Modals?	
We draw a distinction between Dialogs and Modals. 

Modals are for longer content and may include many UI controls, such as accordions or tabs. For shorter confirmation messages or errors, consider using a <a href="../components/dialogs/usage">Dialog</a> instead.

#### Behaviors

Modals are screen-takeover components. 
On desktop, they overlay the screen with a dark color (#000000 with 50% transparency). The modal  is then layered on top of the background to provide high contrast and to prevent the user from interacting with other components. If the user clicks on the transparent overlay, the modal should dismiss. If there is a negative action on the modal (such as 'Cancel'), clicking on the transparency should behave as though the user had taken the negative action.
On mobile, modals take over the entire screen. The user may close the modal by pressing the close button in the top right. 

There is no limit to the length of a modal.
	
### Best Practices
#### Do
* Use for short confirmation messages with affirmative/negative or dismiss actions.

#### Don't
* Use more than one layer of dialog (no dialogs within dialogs).
* Use when there's another 'overlay style' component on the page.
* Allow interactions with other components on the screen while a dialog is active.
	
<img src='https://tmobile.egnyte.com/opendocument.do?entryId=50d60f4f-66e6-426e-aebf-8f09793bf0f9&forceDownload=false&thumbNail=true&w=1200&h=1200&type=proportional' width=600px/>
<img src='https://tmobile.egnyte.com/opendocument.do?entryId=16fc5fdb-d315-414c-abcb-015ea82636dd&forceDownload=false&thumbNail=true&w=1200&h=1200&type=proportional'/>
</div>