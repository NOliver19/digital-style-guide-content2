<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=tts9ybM9Xl&entryId=bf3fcf96-adf9-4673-ba28-09651920990a&cb=1568911659249" width=300px/>
	
### What are Dialogs?
We draw a distinction between Dialogs and Modals. 

Dialogs are for short form confirmation messages with 1-2 buttons. They should not include other UI controls or images. Body text should be limited to approximately 200 characters.

<a href="../components/modals/usage">Modals</a> are for long form content and may include many UI controls, such as accordions or tabs.
	
Dialogs and Modals are both screen-takeover components. They overlay the screen with a dark color (#000000 with 50% transparency). The dialog box is then layered on top of the background to provide high contrast and to prevent the user from interacting with other components. If the user clicks on the transparent overlay, the dialog should dismiss. If there was a negative action on the modal, clicking on the transparency should behave in an equivalent manner.
	
### Best Practices
#### Do
* Use for short confirmation messages with affirmative/negative or dismiss actions. This can include error messages that block the user from continuing their task.

#### Don't
* Use more than one layer of dialog (no dialogs within dialogs).
* Use when there's another 'overlay style' component on the page.
* Allow interactions with other components on the screen while a dialog is active.

### Anatomy
	
<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=tts9ybM9Xl&entryId=8bab174a-4eea-44fb-8439-e93ca0dcdfa3&cb=1568911659250" width=300px/>	
	
<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=tts9ybM9Xl&entryId=f4065e04-47c3-41fa-a163-e421a13cba7d&cb=1568911659249" width=600px/>	
	
</div>