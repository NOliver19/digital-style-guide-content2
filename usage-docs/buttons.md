<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">
	
## **Best practices**
#### Design

* **Use a minimal number of buttons.**
	Strive to create an experience that has one singular point of key focus so that a user has confidence that they know what to expect by pressing it.
* **Button order should remain consistent throughout an experience.**
	Create a predictable pattern that a customer knows to expect.
* When there are multiple buttons, the order of the buttons should be aligned with customers’ intent. Whatever proceeds forward should be on the top (if stacked) and on the right (if side by side).
* Minimum touch target size for a button should be 48x48px. Creating smaller buttons than this can cause usability problems on mobile devices.
* For focus states (reached via screen reader or otherwise) we will use the browser default accessibility behaviors. This is often an outline effect.

#### Copywriting

* Buttons never have more than one line of text
* Use sentence case and no punctuation in button
* Should have clear, easy-to-understand language. Never use overly complicated tech-speak, internal enterprise terms, or industry jargon
* Must clearly communicate what to expect on the next step. Must never give the user a feeling of confusion, hesitation, or doubt.
* The length of a button can vary within reason, but should never exceed more than a few words.
<br>

	
## **Types of buttons**
### Primary

<img src="https://tmobile.egnyte.com/dd/QNDHywBLwg/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true" width=400px />	

These represent the primary action of the page and are magenta. In a linear flow, the primary button should be what allows the user to progress to the next page in the flow. For example: in a page where the user must fill out a form, the primary button should be 'Submit'. There should only be one Primary on a page.

On desktop, primary buttons are 4 columns wide.

<img src="https://tmobile.egnyte.com/dd/RY5FgrRDR6/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true" width=600px/>

On mobile, primary buttons are 12 columns wide.
	
<img src="https://tmobile.egnyte.com/dd/N3UDl09k34/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true" width=300px/>
	
Preferred button sizing for primary/secondary lockups like this is 56px high per button with 16px spacing in between buttons. Buttons should have a clearance of 24px at least to provide a clean touch target. Buttons should be stacked on both desktop and mobile.

There are also black and white primary buttons, but they are not the preferred color. Most designs should not use these, with the exception of marketing related material.

### Secondary

<img src="https://tmobile.egnyte.com/opendocument.do?entryId=d41ce8ef-e391-4564-9716-a3577d57c851&forceDownload=false&thumbNail=true&w=1200&h=1200&type=proportional" width=300px />
	
The preferred secondary button is black and is used only in conjunction with a primary button. In dark themes, this button is white.

<img src='https://tmobile.egnyte.com/opendocument.do?entryId=99adf646-ed54-4a38-b0c6-a22e164b794e&forceDownload=false&thumbNail=true&w=1200&h=1200&type=proportional' width=400px/>	
	
Some marketing pages use a magenta secondary button as a low priority exploratory option. In cases like this, magenta secondary buttons are appropriate. Otherwise, only use secondary buttons in black and when next to a primary button.


### Tertiary

<img src="https://tmobile.egnyte.com/dd/nwMwjpCLtW/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true" width=300px/>

These are used to link out to related tasks that are not the primary focus of the page. For example, linking to an FAQ screen would be a tertiary button. Tertiary buttons can be placed at the end of a label or a paragraph.
	
Magenta is the preferred color for tertiary buttons. In some cases it is appropriate to use black tertiary buttons in order to better structure a page's visual hierarchy.


"Tertiary" is a misnomer because they are frequently used outside of the context of a primary/secondary button lockup.