<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### Overview

Accordions are an organizational tool for text-based information. Accordions are characterized by showing and hiding page content through expansion and collapse.

There is no limit on the number of items you may include in an Accordion.
	
### Best Practices
* Try to keep labels to a single line. In the case that the label must be longer than one line, keep text vertically centered.
* Limit Accordion content to text, hyperlinks, or tertiary buttons. Do not include other controls or images.
* The icon should be one of: a plus, minus, upward or downward chevron.
* Accordions should be 6 responsive columns wide on desktop and 12 on mobile. This means on desktop, there can be two accordions side by side.
* Accordions should only contain text, links, and tertiary buttons.