<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

## Type of notifications
### Message bars

Message bars are used to draw the user's attention to high-importance information. Message bars span the full width of the page. They exist underneath the header. If there is a progress bar, they may overlap the progress bar. Other page content should be pushed down. Message bars may contain a combination of text, icon, and tertiary button. They cannot contain any other controls.

There are three different variations of message bars:

#### Magenta (Preferred)
Use this in most cases. Appropriate messaging topics include:
* Promotional materials
* New features
* Affirmative messaging
* Benefits added to account

#### Gray
Use this for banner content that does not demand the user's attention. (Remove?)

#### Red
Use this for medium-priority errors.
* Autopay failure.
* Account suspension.
* Bill overdue.
* Other account-level errors that demand the user's attention.

</div>



