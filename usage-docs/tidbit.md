## What is a Tidbit?

Tidbits draw the user’s attention to an important piece of information. They contain text (with or without an image), are contained in a box, and have a magenta line on the left-hand side.

Tidbits are not Banners. They do not span the full screen width and are mixed in with regular page content. 

A Tidbit can contain an image, but the focus should be on the text. 
	
## Best practices
	
### Do
* Use a Tidbit for information regarding changes to account or billing
* Use a Tidbit for low priority warning messages
* Use a Tidbit for statistics or other information relevant to a user's decision
	
### Don't
* Use Tidbit for high priority errors