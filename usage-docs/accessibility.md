<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">


## Accessibility

Accessibility was mandated by the U.S. government as part of The Americans with Disabilities Act or ADA. The ADA is a civil rights law that prohibits discrimination against individuals with disabilities in all areas of public life, including jobs, schools, transportation, and all public and private places that are open to the general public.

In 1998, Congress amended the Rehabilitation Act of 1973 with Section 508, that requires Federal agencies to make their electronic and information technology (EIT) accessible to people with disabilities. The World Wide Web Consortium (W3C), established in 1994, regulates industry members for the compatibility of standards for the World Wide Web. To address the growing divide between rising Internet usage and its limited accessibility among individuals with disabilities, W3C created the “Web Accessibility Initiative: Web Content Accessibility Guidelines” (WAI-WCAG) to address this issue. The guidelines were originally published in 1999, and consisted of 14 points for accessible design for individuals with different types of disabilities.

WCAG 2.1 encompasses Section 508 and extends the criteria internationally.

### A11y

A11y, pronounced "A eleven Y", is numeronym that represents the first and last letters of the word Accessibility and the 11 letters that fall between them.

### Why A11y?

Not only can T-Mobile suffer litigation for not adhering to A11y standards, creating the best user experience for all clientele is paramount to keeping T-Mobile the industry leader.
	
	
</div>