<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">


### What are dropdowns?

Dropdowns are a selection control. While closed, Dropdowns display an active selection from a set of possible options. On desktop, clicking on the selected option opens a menu from which the user may select a different option. On mobile, use an OS-appropriate selection tool.

iOS: https://developer.apple.com/design/human-interface-guidelines/ios/controls/pickers/
Android: https://material.io/components/menus/

### Best practices
#### Do
* Use dropdowns when there are more than 3 options available. Otherwise, consider using radio buttons instead.
* Consider if your dropdown could be replaced by a text field (ex: entering birth year)
* Support keyboard input for quick selection

#### Don’t
* Use dropdowns excessively. There are many components that can solve the same problem as a dropdown more elegantly. The advantage of dropdowns is their small footprint.
</div>