<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">


### What are Radio Buttons?

Radio buttons are a selection control that allows the user to select one option from a number of other mutually exclusive options. They are signified by a stylized circle control and a label.

### Best practices

#### Do
* Control should be to the left of the label. 
* The label should also be interactive.
* Radio buttons must always have a default selection.

#### Don’t
* Do not stack radio buttons horizontally. 

In the case that a label for a checkbox or radio button has a paragraph description, the selection control should be aligned with the label. Example:

### Anatomy
* The spacing between the label and radio button is 16px. 
* In all cases, selection controls should be stacked vertically.
	
</div>