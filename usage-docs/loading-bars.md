<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What are Loading bars?
	
Loading bars provide feedback to the user that a request is currently being processed. Loading bars appear underneath the header. There are two types of loading bars:
* Determinate loading bars
 * Use determinate loading bars when the operation will take a known amount of time to complete.
* Indeterminate loading bars
 * Use indeterminate loading bars when the operation will take an unknown amount of time to complete.

### Best practices	
	
#### Do
* Use loading bars when an operation takes longer than 1/10th of a second to complete. https://www.nngroup.com/articles/response-times-3-important-limits/
* Use Loading bars for page-level navigation load times

#### Don’t
* Position loading bars anywhere other than the header. If you find yourself needing to position a loading bar elsewhere, consider using a Spinner instead.
</div>