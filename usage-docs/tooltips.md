<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">


### What are Tooltips?

Use tool tips to provide definitions or additional information about text-based content in page.

Tooltips display on hover or on press. On mobile, because there is no hover concept, tooltips display on press.

When focused by a screen reader or otherwise, Tooltips should show their content.

### Best practices

* Tooltips should not contain links or controls
* Tooltips should only contain text
* Do not place a tooltip within a tooltip
* Limit tool tips in length
* Use a maximum of one tooltip per paragraph.
* Be mindful that minimum touch target size should be 48x48px.
* The tooltip 'arrow' should point to the center of the underlined text segment.

	
</div>
