## Discrete editing...
```
HTML file: form-elements.component.html

<mat-slider
  [disabled]="true"
  [max]="max"
  [min]="min"
  [step]="step"
  [thumbLabel]="thumbLabel"
  [tickInterval]="tickInterval"
  [(ngModel)]="value">
</mat-slider>
```

## Continuous

HTML file: form-elements.component.html

```
<mat-slider color="primary"></mat-slider>
```    

