<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

## **Best practices**

**Design**
* Use to enter information, make selections, filter content, or trigger actions. 
* Should appear dynamically as a group of multiple interactive elements. 
* Should not be used like buttons, which should be a consistent and familiar call to action.
	
**Copywriting**
* Should not be more than 25 characters
* Use sentence case and no punctuation 
* Should be clear and easy-to-understand language
