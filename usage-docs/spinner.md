<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What is a Spinner?

Spinners provide feedback to the user that a request is currently being processed. There are two types of Spinners:
* Determinate Spinners
 * Use determinate Spinners when the operation will take a known amount of time to complete.
* Indeterminate Spinners
 * Use indeterminate Spinners when the operation will take an unknown amount of time to complete.

#### Do
* Use Spinners when an operation takes longer than 1/10th of a second to complete. https://www.nngroup.com/articles/response-times-3-important-limits/

#### Don’t