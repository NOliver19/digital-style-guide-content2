<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What are cards?
	
Cards are UI components that segment content through use of a background-foreground pattern, like playing cards lying on a table.

The homepage uses cards to segment different categories, "Balance", "My line", etc.

### Best Practices
	
#### Do

* Use cards to effectively lay out information & actions of discrete types
* Each card should have one subject
* Cards should be of similar importance to user tasks
* Put UI controls inside cards

#### Don’t

* Put multiple subjects into a single card
	
</div>