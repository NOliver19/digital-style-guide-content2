<style>
	.iconBox {
		width: 48px;
		height: 48px;
	}
</style>
<style>
 .iconBox img {
	 width: 100%;
   height: 100%;
	}
</style>


<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">
	
## Best practices

T-Mobile follows material guidelines for icon styling. If you can't find an icon you're looking for in this style guide, check the [Material Icon Library](https://material.io/resources/icons).

Our preferred icon size for actionable buttons is 24 x 24px. For consistency, try to make your icons this size. In the case where the icon is not actionable, it can be any size. Our icon ramp follows:

* 16 x 16 px
* 24 x 24 px
* 40 x 40 px

In almost all cases, actionable icons should be used with a label. There are a few exceptions to this rule:

* Menus
* Icons within a floating action button

Actionable icons can be black, grey, or magenta. Warning icons may be used in red. On dark backgrounds, icons may be white. Icons cannot be used in any other color.
	
Future content: when to use illustration vs icon.
</div>