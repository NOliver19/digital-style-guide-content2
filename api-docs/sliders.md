### Discrete

Module added to mat-components.module.ts:

```
import { MatSliderModule } from '@angular/material/slider';
 ```     

### Continuous

HTML file: form-elements.component.html

```
<mat-slider color="primary"></mat-slider>
```      
